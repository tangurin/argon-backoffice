import Vue from 'vue'
import Vuesax from 'vuesax'
import { colors, config } from '@/themeConfig.js'

Vue.use(Vuesax, {
  theme: {
    colors,
  },
  rtl: false,
})

import VxTooltip from '@/layouts/components/vx-tooltip/VxTooltip.vue'
import VxBreadcrumb  from '@/layouts/components/VxBreadcrumb.vue'
import VxCard  from '@/components/vx-card/VxCard.vue'
import VxList  from '@/components/vx-list/VxList.vue'
import FeatherIcon  from '@/components/FeatherIcon.vue'
import VxInputGroup  from '@/components/vx-input-group/VxInputGroup.vue'

// mock
import '@/fake-db/index.js'

Vue.component(VxTooltip.name, VxTooltip)
Vue.component(VxCard.name, VxCard)
Vue.component(VxList.name, VxList)
Vue.component(VxBreadcrumb.name, VxBreadcrumb)
Vue.component(FeatherIcon.name, FeatherIcon)
Vue.component(VxInputGroup.name, VxInputGroup)

Vue.filter('k_formatter', function (num) {
  return num > 999 ? `${(num / 1000).toFixed(1)}k` : num
})

// v-select component
import vSelect from 'vue-select'

// Set the components prop default to return our fresh components
vSelect.props.components.default = () => ({
  Deselect: {
    render: createElement => createElement('feather-icon', {
      props: {
        icon: 'XIcon',
        svgClasses: 'w-4 h-4 mt-1'
      }
    })
  },
  OpenIndicator: {
    render: createElement => createElement('feather-icon', {
      props: {
        icon: 'ChevronDownIcon',
        svgClasses: 'w-5 h-5'
      }
    })
  }
})

Vue.component(vSelect)

