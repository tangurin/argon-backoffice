export default function ({ $axios, redirect }) {

    if (process.env.NODE_ENV != 'development') {
      return;
    }

    //$axios.onRequest(config => {
    //    console.log('Making request to ' + config.url)
    //})

    $axios.onError((error) => {
        console.log('');
        console.log(`--------- Axios error ---------`);
        console.log('');
        if (error.response) {
          // Request made and server responded
          //console.log(error.response);
          console.log(error.message);
          console.log(error.config.url);
          console.log(error.response.data.message);
          if (error.response.data.file) {
            console.log(error.response.data.file + ' - Line: ' + (error.response.data.line || ''));
          }
        } else if (error.request) {
          // The request was made but no response was received
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }

        console.log('');
        console.log('--------- //Axios error ---------');
        //const code = parseInt(error.response && error.response.status)
        //if (code === 400) {
        //    redirect('/400')
        //}
    })
}
